import psycopg2
import os

url = os.environ.get("POSTGRES_URL")

def execute_query(query):
    try:
        connection = psycopg2.connect(url)
        cursor = connection.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        connection.commit()

        return result
    
    except Exception as e:
        print(f"Ошибка: {e}")
    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()


query_1 = """select 
                last_name,
                first_name,
                middle_name, 
                birth_date 
            from 
                students 
            order by 
                    birth_date 
            desc limit 1;
        """

query_2 = """select 
                last_name,
                first_name,
                middle_name, 
                birth_date 
            from 
                students 
            order by 
                    birth_date 
            asc limit 1;
        """

results_1 = execute_query(query_1)

results_2 = execute_query(query_2)

print("Самый младший студент")
print(results_1)

print("Самый старший студент")
print(results_2)
