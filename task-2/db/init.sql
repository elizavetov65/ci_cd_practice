create table students (
	id serial primary key,
	first_name varchar(50),
	middle_name varchar(50),
	last_name varchar(50),
	birth_date date,
	exams varchar(100)
);

insert into students(last_name, first_name, middle_name, birth_date, exams) values
('Левин', 'Алексей', 'Михайлович', '2001-11-27', 'Биология, Химия, Высшая математика'),
('Родионова', 'Ирина', 'Глебовна', '2000-08-20', 'Высшая математика, Химия, Биология'),
('Моисеев', 'Марк', 'Львович', '2001-01-29', 'Физика, Философия, Экономика'),
('Шевцов', 'Ярослав', 'Викторович', '2002-09-19', 'Философия, Литература, Русский язык'),
('Горшков', 'Даниил', 'Иванович', '2002-06-22', 'Экономика, Философия, Русский язык'),
('Губанов', 'Георгий', 'Григорьевич', '2002-10-02', 'Экономика, Высшая математика, Русский язык'),
('Куликова', 'Анна', 'Арсентьевна', '2003-06-08', 'Физика, Химия, Биология'),
('Фролова', 'Полина', 'Андреевна', '2000-04-04', 'Высшая математика, Физика, Философия'),
('Николаев', 'Алексей', 'Алексеевич', '2002-03-17', 'Экономика, Высшая математика, Биология'),
('Павлова', 'Алиса', 'Матвеевна', '2001-05-25', 'Химия, Русский язык, Высшая математика');

