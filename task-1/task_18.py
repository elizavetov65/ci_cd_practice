import os

def result(num):
    octal_number = oct(int(num))
    hex_number = hex(int(num))
    return octal_number, hex_number
    

if __name__ == "__main__":
    num = int(os.environ.get("N_ELEMS"))
    octal_number, hex_number = result(num)
    print(f"В шестнадцатеричной системе: {hex_number[2:]}\nВ восьмиричной системе: {octal_number[2:]}")
